package br.edu.oodb;

public class Pesssoa {

	private String nome;
	
	private String sobrenome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	
	public void imprimir(){
		System.out.println("Impressão da Classe Pessoa");
		System.out.println("Nome: "+nome);
		System.out.println("Sobrenome: "+sobrenome);
	}
	
}
